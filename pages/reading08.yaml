title:      "Reading 08: Corporate Conscience"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week focus on [corporate personhood] and [antitrust]:

  ### Corporate Conscience

  - [How Corporations Got The Same Rights As People (But Don’t Ever Go To Jail)](https://consumerist.com/2014/09/12/how-corporations-got-the-same-rights-as-people-but-dont-ever-go-to-jail/)
  
  - [This Is the Hidden Nazi History of IBM — And the Man Who Tried to Expose It](https://mic.com/articles/142991/edwin-black-ibm-nazi-holocaust-history)

  - [Tech workers pledge to never help Trump build Muslim registry](http://money.cnn.com/2016/12/13/technology/never-again-data-collection-trump/index.html)

  - [Why Tech Worker Dissent is Going Viral](https://www.wired.com/story/why-tech-worker-dissent-is-going-viral/) (**mirror**: [outline](https://outline.com/6AZtcm))

  - [The Techlash Has Come to Stanford](https://slate.com/technology/2019/08/stanford-tech-students-backlash-google-facebook-palantir.html)
  
  ### Antitrust

  - [How Google and Amazon Got Away With Not Being Regulated](https://www.wired.com/story/book-excerpt-curse-of-bigness/)

  - [Long antitrust saga ends for Microsoft](http://www.seattletimes.com/business/microsoft/long-antitrust-saga-ends-for-microsoft/)
  
  - [EU Hits Google With Record $2.7 Billion Antitrust Fine](https://www.theatlantic.com/news/archive/2017/06/eu-google-fine/531750/)

  - [Tech and Antitrust](https://stratechery.com/2019/tech-and-antitrust/)

  - [The Right Way to Regulate Big Tech](https://www.nationalreview.com/2019/08/big-tech-regulation-right-way/)

  ### Optional: Corporate Personhood

  These articles discuss the idea of [corporate personhood].

  - [If Corporations Are People, They Should Act Like It](https://www.theatlantic.com/politics/archive/2015/02/if-corporations-are-people-they-should-act-like-it/385034/)

  - [Corporate Personhood’s Long Life](http://www.nationalreview.com/article/365425/corporate-personhoods-long-life-paul-moreno)

  - [What is Corporate Social Responsibility?](https://www.businessnewsdaily.com/4679-corporate-social-responsibility.html)

  ### Optional: Antitrust

  These articles discuss the recent growing discussion over [antitrust] regulation.
  
  - [Watch out, Google, Amazon, and Facebook: the Justice Department just launched a major antitrust review](https://www.vox.com/recode/2019/7/23/20707708/justice-department-antitrust-big-tech-google-facebook-amazon)

  - [The techlash against Amazon, Facebook and Google—and what they can do](https://www.economist.com/news/briefing/21735026-which-antitrust-remedies-welcome-which-fight-techlash-against-amazon-facebook-and)

  - [Are Google, Amazon and others getting too big?](http://www.bbc.com/news/business-39875417)

  ### Case Study: IBM and the Holocaust

  These articles describe IBM's involvement in the Holocaust:

  - [IBM Statement on Nazi-era Book and Lawsuit](http://archive.is/U6Rx#selection-1825.0-1825.42)

  - [IBM and the Holocaust](http://www.nytimes.com/books/first/b/black-ibm.html)

  - [I.B.M.'s Sales to the Nazis: Assessing the Culpability](http://www.nytimes.com/2001/03/07/books/books-of-the-times-ibm-s-sales-to-the-nazis-assessing-the-culpability.html)

  ### Case Study: Muslim Registry

  These articles describe a recent pledge to avoid building a [Muslim Registry]
  in the US:

  - ["Muslim registries", Big Data and Human Rights](https://www.amnesty.org/en/latest/research/2017/02/muslim-registries-big-data-and-human-rights/)

  - [Apple, Google, and Uber join list of tech companies refusing to build Muslim registry](http://www.theverge.com/2016/12/16/13990234/google-muslim-registry-refusal-donald-trump-silicon-valley)

  - [America Already Had a Muslim Registry](https://www.theatlantic.com/technology/archive/2016/12/america-already-had-a-muslim-registry/511214/)

  ### Case Study: Microsoft Antitrust

  These articles discuss the historic [Microsoft Antitrust] case:
  
  - [‘Crush Them’: An Oral History of the Lawsuit That Upended Silicon Valley](https://www.theringer.com/tech/2018/5/18/17362452/microsoft-antitrust-lawsuit-netscape-internet-explorer-20-years)
  
  - [U.S. VS. MICROSOFT: The Overview; U.S. Judge Says Microsoft Violated Antitrust Laws With Predatory Behavior](http://www.nytimes.com/2000/04/04/business/us-vs-microsoft-overview-us-judge-says-microsoft-violated-antitrust-laws-with.html)

  - [The eternal antitrust case: Microsoft versus the
    world](https://arstechnica.com/tech-policy/2010/09/the-eternal-antitrust-case-microsoft-versus-the-world/)

  ### Case Study: Google Antitrust

  These articles discuss the recent [Google Antitrust] cases:

  - [How did Google get so big?](https://www.cbsnews.com/news/how-did-google-get-so-big/)

  - [The Case Against Google](https://www.nytimes.com/2018/02/20/magazine/the-case-against-google.html)

  - [Google’s iron grip on Android: Controlling open source by any means necessary](https://arstechnica.com/gadgets/2018/07/googles-iron-grip-on-android-controlling-open-source-by-any-means-necessary/)

  Here are some related podcasts:

  - ['World Without Mind': How Tech Companies Pose An Existential Threat](http://www.npr.org/sections/alltechconsidered/2017/09/11/550177421/world-without-mind-how-tech-companies-pose-an-existential-threat)

  - [Episode 787: Google Is Big. Is That Bad?](http://www.npr.org/sections/money/2017/08/04/541643346/episode-787-google-is-big-is-that-bad)

  ## Quiz

  Once you have completed the readings, fill out the following [quiz]:

  <div class="text-center">
  <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeihC6QldwjkosNb12h844jXnmbAWGLtezcVPzJQylLXzeeAw/viewform?embedded=true" width="640" height="1173" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
  </div>

  <div class="alert alert-danger" markdown="1">
  #### <i class="fa fa-warning"></i> Notre Dame Login

  To view and submit the form below, you need to be logged into your Notre Dame
  Google account.  The easiest way to do this is to login to
  [gmail.nd.edu](https://gmail.nd.edu) and then visit this page in the same
  browser session.

  </div>

  **Note**, you can view the initial quiz score after you submit your
  responses.  If you get any answers wrong, you can go back and adjust your
  answers as necessary.  After the deadline has passed, any wrong answers
  will be given partial credit.

  [quiz]:   https://forms.gle/DZYkSj2WuNKiBVJJ6

  [Corporate Personhood]:   https://en.wikipedia.org/wiki/Corporate_personhood
  [Corporate Social Responsibility]: https://en.wikipedia.org/wiki/Corporate_social_responsibility
  [antitrust]:              https://en.wikipedia.org/wiki/Competition_law
  [IBM and the Holocaust]:  https://en.wikipedia.org/wiki/IBM_and_the_Holocaust
  [Muslim Registry]:        https://en.wikipedia.org/wiki/National_Security_Entry-Exit_Registration_System
  [Microsoft Antitrust]:    https://en.wikipedia.org/wiki/United_States_v._Microsoft_Corp.
  [Sony Rootkit]:           https://en.wikipedia.org/wiki/Sony_BMG_copy_protection_rootkit_scandal
  [Google Antitrust]:       https://en.wikipedia.org/wiki/European_Union_vs._Google
